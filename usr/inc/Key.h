#ifndef		__KEY_H__
#define		__KEY_H__


#define key_no	   		0    	//no keys
#define key_click  		1    	//click keys
#define key_double 		2    	//double click
#define key_long   		3    	//long click

#define key_state_0			0
#define key_state_1   	1
#define key_state_2   	2
#define key_state_3   	3 	//key states define


void key_need_detection(void);
void key_task(void);
UINT8 key_read(void);
void key_default();


#endif