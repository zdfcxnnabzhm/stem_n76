/********************************** (C) COPYRIGHT *******************************
* File Name          : WS281X.H											*	
* Author             : OAZON                                                   *
* Version            : V1.0                                                     *
* Date               : 2018/06/15                                               *
* Description        : 
********************************************************************************/

#ifndef	__WS281X_H__
#define __WS281X_H__


#include "Common.h"

#define 	WS_LED_NUM	 	(1)				/*WS灯的数量*/
//#define 	WS_DAT_LEN	 	(WS_LED_NUM*3)		       /*WS数据的长度*/

#define  	ring_brightness_duty   		 255

/****************************硬件IO配置*********************************/
sbit LED_RING = P0^4;
#define 	WS_IO_LOW() 	(LED_RING=0)
#define 	WS_IO_HIGH() 	(LED_RING=1)

/****************************名称统一宏定义*********************************/
#define  U8 	UINT8
#define  U16 	UINT16
#define  U32 	UINT32

#define  U8X 	  UINT8X				/*不同平台的xdata定义不同，需要修改宏*/
#define  U16X 	UINT16X
#define  U32X 	UINT32X

/****************************函数定义*********************************/

U8 WS_frame_asyn(U8* Ptr);
U8 WS_frame_sync(U8* Ptr);

void ring_display_clear();
#endif
/**************************** END *************************************/