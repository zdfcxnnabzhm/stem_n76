#include "N76E003.h"
#include "SFR_Macro.h"
#include "define.h"
#include "WS281X.h"
#include "Timer.h"
#include "Config.h"
#include "Brightness.h"
#include "Key.h"
#include "LED.h"
#include "CLED.h"
#include "Buzzer.h"
#include "Servo.h"

UINT16 Time1flag = 0;														//总时间
UINT16 Interval = 0;															//时间间隔
#ifdef MOD_ULTRA																
#define DOUBLE_CRYSTAL_FREQ  16
/*
*  @brief  	超声波传感器定时器0初始化
*  @param[in]  
*  @return	
*  @note
*/
UINT8 mbDelay10H, mbDelay10L; 
void Timer0_init(void)
{
		UINT16 iTmp = 0;
	
		TMOD |=0x01;
		TH0=0;
		TL0=0; 
		TR0=0; 
		ET0=1;
		EA=1; 

//	iTmp = (UINT16)(65536-(7500/(12/DOUBLE_CRYSTAL_FREQ))));	
		iTmp = (UINT16)(65536-((7500/12)/DOUBLE_CRYSTAL_FREQ));
		mbDelay10H = iTmp >> 8; 
		mbDelay10L = iTmp &0x0F; 
}
/*
*  @brief  	返回定时器0高字节
*  @param[in]  
*  @return	
*  @note
*/
UINT8 get_th0(void)
{
		return mbDelay10H;
}
/*
*  @brief  	返回定时器0低字节
*  @param[in]  
*  @return	
*  @note
*/
UINT8 get_tl0(void)
{
		return mbDelay10L;
}
bit mbDelayOverFlg = 1;	
/*******************************************************************************
mbDelayOverFlg = 1;设定延时结束
*******************************************************************************/
void Timer0_ISR (void) interrupt 1 
{	
		TR0 = 0; 
		mbDelayOverFlg = 1; 			
}
/*
*  @brief  	返回超声波模块延迟程序控制标记
*  @param[in]  
*  @return	
*  @note
*/
UINT8 get_sonar_delay_end(void)
{
		return mbDelayOverFlg;
}
/*
*  @brief  	将超声波模块延迟程序控制标记清0
*  @param[in]  
*  @return	
*  @note
*/
void sonar_delay_clear(void)
{
		mbDelayOverFlg = 0; 
}

#else

/*
*  @brief  定时器0初始化,1ms
*  @param[in] 
*  @return
*  @note
*/
static UINT8 TH0_Tmp,TL0_Tmp;
void timer0_init()
{
		TIMER0_MODE1_ENABLE;	
		
		TH0_Tmp = (65536-TH0_INIT)/256;
		TL0_Tmp = (65536-TL0_INIT)%256; 
		TH0 = TH0_Tmp;
		TL0 = TL0_Tmp;
	
		set_ET0;                                  
		set_EA;  
		set_TR0;
}
/*******************************************************************************
* Function Name  : Timer0_ISR ()
* Description    : n76定时器0中断处理函数
*******************************************************************************/
UINT16 Tim0flag = 0;
void Timer0_ISR (void) interrupt 1 
{
		TH0 = TH0_Tmp;
		TL0 = TL0_Tmp;    
		
		Tim0flag++;												//时间延时	
#ifdef MOD_ADC			
		if(Tim0flag % 50 == 0) {	
				adc_need_detection();		
		}
#endif			
		
#ifdef MOD_KEY		
		if(Tim0flag % 10 == 0) {	
				key_need_detection();		
		}
#endif			
		
#ifdef MOD_SERVO		
		if(Tim0flag % get_servo_delay() == 0) {	
				set_servo_need_down();		
		}		
#endif		
}


#endif
/*
*  @brief  定时器1初始化,50ms进一次中断，用于蜂鸣器，灯
*  @param[in] 
*  @return
*  @note
*/
void timer1_init()
{
		TIMER1_MODE1_ENABLE;
		clr_T1M;
		
			TH1 = 0;
			TL1 = 0;
	
			set_ET1;                                   
			set_EA;  
			set_TR1; 	
}
/*******************************************************************************
* Function Name  : Timer1_ISR ()
* Description    : n76定时器1中断处理函数
*******************************************************************************/
void Timer1_ISR (void) interrupt 3 		 	//定时器1中断，用于延时
{
    TH1 = 0;
    TL1 = 0;						   							//50ms进一次中断
    Time1flag++;                     		//P0.3 toggle when interrupt
		Interval++;
		
#ifdef MOD_LED	
		if(Interval % get_led_time_interval() == 0) {
				set_led_update_flag();
		}
#endif
		
#ifdef MOD_CLED
		if(Interval % get_cled_time_interval() == 0) {
				set_cled_update_flag();
		}	
#endif		
	
#ifdef MOD_BUZZER
		if(Interval % get_buzzer_time_interval() == 0) {
				set_buzzer_update_flag();
		}	
#endif		
		
}







