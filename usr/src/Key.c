#include "N76E003.h"
#include "Common.h"
#include "Key.h"
#include "I2C.h"
#include "config.h"
#include "string.h"

#ifdef  MOD_KEY																//按键
sbit key_input = P1^1;
#define key_init()	(key_input =1)							//按键IO口

/*
*  @brief		按键检测，1需要检测
*  @param[in] 
*  @return	
*  @note
*/
UINT8 key_detection = 0;
void key_need_detection(void)
{
		key_detection = 1;
}
UINT8 get_key_detection(void)
{
		return key_detection;
}

/*
*  @brief		按键不同按键方式，做不同处理
*  @param[in] 
*  @return	返回按键按下（单，双，长）状态	
*  @note
*/
static UINT8 key_driver(void)
{
		static UINT8 key_state_buffer1 = key_state_0;
		static UINT8 key_timer_cnt1 = 0;
		UINT8 key_return = key_no;
		UINT8 key;
		
		key = key_input;  
		
		switch(key_state_buffer1)
		{
				case key_state_0:
					if(0 == key)
						key_state_buffer1 = key_state_1; 			
						//按键被按下，状态转换到按键消抖和确认状态//
					break;
				case key_state_1:
					if(0 == key)
					{
						key_timer_cnt1 = 0;
						key_state_buffer1 = key_state_2;
						//按键仍处于按下状态
						//消抖后key_timer_cnt1用于计时
					}
					else
						key_state_buffer1 = key_state_0;
						//按键抬起，则回到初始状态
					break;  				//消抖完成
				case key_state_2:
					if(1 == key) 
					{
						key_return = key_click;  									//按键抬起，产生一次click操作
						key_state_buffer1 = key_state_0;  				//转换到按键初始状态
					}
					else if(++key_timer_cnt1 >= 100)  					//按下计时超过1000ms
					{
						key_return = key_long;  									//长按事件
						key_state_buffer1 = key_state_3;  				//等待释放
					}
					break;
				case key_state_3:  
					if(1 == key) 
						key_state_buffer1 = key_state_0;  
					break;
		}
		return key_return;
}
/*
*  @brief		按键处理函数
*  @param[in] 
*  @return	返回无键，单击，双击，长按状态
*  @note
*/
UINT8 key_read(void)
{
	static UINT8 key_state_buffer2 = key_state_0;
	static UINT8 key_timer_cnt2 = 0;
	UINT8 key_return = key_no;
	UINT8 key;
	
	key = key_driver();
	
	switch(key_state_buffer2)
	{
		case key_state_0:
			if(key == key_click)
			{
				key_timer_cnt2 = 0;  										//第一次单击，在下个状态判断是否出现双击再返回
				key_state_buffer2 = key_state_1;
			}
			else 																			//无键和长按返回
				key_return = key;  
			break;
		case key_state_1:
			if(key == key_click)  										//再次单击，500ms内
			{
				key_return = key_double;  							//返回双击事件，并回到初始值
				key_state_buffer2 = key_state_0;
			}
			else if(++key_timer_cnt2 >= 50)
			{
				//如果500ms内没有单击事件，则返回单击					
				key_return = key_click;  
				key_state_buffer2 = key_state_0;  
					
			}
			break;
	}
	return key_return;
}
/*
*  @brief		05寄存器比较特殊，有三个值，第0位1为是代表小于，2是大于，第1位是代表要比较的数值，
						第2位是代表满足条件后02寄存器要写入的值，
*  @param[in] 
*  @return	
*  @note
*/
void key_default()
{
	if(get_stat_reg() == REST_MARK)						//如果02寄存器已经被置值则不进入判断
	{																		
		switch(store_5[0])
		{
			case DEFA_LESS:
						if(store_5[1] <= store_8[0])
							set_stat_reg(store_5[2]);
						break;
			case DEFA_OVER:
						if(store_5[1] >= store_8[0]) {
							set_stat_reg(store_5[2]);		
							memset(store_5,0x00,3);
						}
						break;
			default:
						break;					
		}
	}
}
void key_task(void)
{
		if(get_key_detection() == 0) {
				return;
		}
		key_data();
		key_detection = 0;
}
#endif		//按键
