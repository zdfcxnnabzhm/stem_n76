#include "N76E003.h"
#include "SFR_Macro.h"
#include "stdio.h"
#include "Define.h"
#include "WS281X.h"
#include "config.h"
#include "Timer.h"
#include "PWM.h"
#include "I2C.h"
#include "buzzer.h"
#include "Key.h"
#include "Sonar.h"
#include "Brightness.h"
#include "Motor.h"
#include "Servo.h"
#include "Dht11.h"
#include "Cled.h"
#include "LED.h"

/*
*  @brief  	模块初始化
*  @param[in]  
*  @return	
*  @note
*/
void sys_init(void)	
{
#ifdef VIA_IIC
	 Init_I2C();															//i2c初始化	
#endif	
#ifdef MOD_KEY															//按键
	 
#endif
#ifdef MOD_SERVO
	pwm_servo_init();													//舵机
#endif
#ifdef MOD_ADC
	brightness_init();												//光敏初始化
#endif	
#ifdef MOD_MOTOR
	motor_init();															//电机初始化
#endif	
#ifdef MOD_BUZZER														//蜂鸣器初始化								
	buzzer_pwm_init();	
	get_buzzer_volume_min();	
#endif	
#ifdef MOD_CLED
																						//RGB灯  	50ms进一次定时器中断
#endif	
#ifdef MOD_LED
	led_pwm_init();														//led单色灯
	get_led_pwm_min();										
#endif	
#ifdef MOD_ULTRA

//	InitUltrasonicDistance();								//超声波模块
#endif
#ifdef MOD_IR
	infrared_init();
#endif
}
/*
*  @brief  	中断优先级设置，iic中断最高
*  @param[in]  
*  @return	
*  @note
*/
void Priority_level(void)
{
#if 1
		clr_PT0;										//定时器0中断优先级0
		clr_PT0H;
		set_PT1;										//定时器1中断优先级1
		clr_PT1H;
#endif
#if 1
		set_PI2C;
		set_PI2CH;									//iic中断优先级3最高
#endif 
}

void main(void)
{
	Set_All_GPIO_Quasi_Mode;	
	
	timer0_init();
	timer1_init();
	InitialUART0_Timer3(115200);										//串口打印
	Priority_level();																//优先级设置
	sys_init();																			//模块初始化
	
	while(1)
	{
#ifdef MOD_BUZZER																	//蜂鸣器
		i2c_buzzer();																	
		buzzer_task();
#endif	
#ifdef	MOD_KEY																		//按键
		key_task();
		i2c_key();
		key_default();
#endif		
#ifdef MOD_ULTRA																	//超声波模块
		ultra_task();
		i2c_ultra();
#endif		
#ifdef MOD_ADC																		//光敏
		brightness_task();
		i2c_adc();
#endif		
#ifdef MOD_MOTOR																	//电机
		i2c_motor();
		motor_task();
#endif
#ifdef MOD_SERVO																	//舵机
		i2c_servo();																	
		servo_task();
#endif
#ifdef MOD_CLED																		//彩灯
		i2c_cled();
		cled_task();
#endif
#ifdef MOD_LED																		//单色灯
		i2c_led();
		led_task();
#endif
	}
}














