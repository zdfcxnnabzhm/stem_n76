#ifndef	_PWM_H_
#define	_PWM_H_
#include "Config.h"

#define SetPWMPDat(dath,datl)			PWMPH = dath;PWMPL = datl													//设置PWM周期寄存器
#define SetPWM0Dat(dath,datl)			PWM0H = dath;PWM0L = datl													//设置PWM占空比
#define SetPWM1Dat(dath,datl)			PWM1H = dath;PWM1L = datl
#define SetPWM2Dat(dath,datl)			PWM2H = dath;PWM2L = datl
#define SetPWM3Dat(dath,datl)			PWM3H = dath;PWM3L = datl
#define SetPWM4Dat(dath,datl)			set_SFRPAGE;PWM4H = dath;PWM4L = datl;clr_SFRPAGE
#define SetPWM5Dat(dath,datl)			set_SFRPAGE;PWM5H = dath;PWM5L = datl;clr_SFRPAGE

#ifdef MOD_LED	
void led_pwm_init(void);
void led_pwm_set(UINT8 color_h,UINT8 color_l);
void get_led_pwm_min(void);
void led_pwm_Trans(UINT8* color);
void led_off(void);
//void pwm_set(UINT8 rh,UINT8 rl,UINT8 gh,UINT8 gl,UINT8 bh,UINT8 bl);
#endif

#ifdef MOD_BUZZER
UINT16 get_buzzer_volume(UINT8* temp);
void buzzer_on(UINT16 *temp);
void buzzer_off();
void buzzer_pwm_init();
void get_buzzer_volume_min(void);
#endif

void pwm_servo_init();



#endif //__PWM_H__