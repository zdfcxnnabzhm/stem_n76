#include "N76E003.h"
#include "SFR_Macro.h"
#include "Define.h"
#include "WS281X.h"
#include "I2C.h"
#include "Config.h"
#include "Dht11.h"
#include "Timer.h"
#include <intrins.h> 

#ifdef MOD_DHT11																										//温湿度传感器

sbit DHT = P1^1;																										//输入引脚


void delay_1(UINT16 k)
{
		Tim0flag = 0;
		while(Tim0flag<=k);
}

/* @brief  将接收到的电平拼凑成一个字节
					 位数据0的格式为：50微秒的低电平和26-28的高电平
					 位数据1的格式为：50微秒的低电平加70微秒的高电平		
*  @param[in] 
*  @return	
*  @note
*/
UINT8 getByte()
{
    unsigned char i, timer, ret;
    ret = 0;
    for(i = 0; i < 8; ++i){
        timer = 1;
        while(!DHT && (++timer));																		//50us低电平
				Delay10us();
        Delay10us();
        Delay10us();
	
        ret <<= 1;																									//此时若IO口为高电平，则为1
        ret |= DHT;
        timer = 1;
			
        while(DHT && (++timer));																		//等待高电平结束
    }
    return ret;
}
/* @brief  获取温湿度传感器数据，并校验		
*  @param[in] 
*  @return	
*  @note
*/
U8 dht11_task(void)
{
		UINT8 temp_H,temp_L,RH_H,RH_L,cksum,tmp;
    UINT8 timer;
		UINT8 gTempe=0;
		UINT8 gHuimi=0;	
	
    DHT = 0;
    delay_1(18);
    DHT = 1;
    Delay10us();
    Delay10us();
    Delay10us();
    Delay10us();
    DHT = 1;
	
    if(0 == DHT) {
        timer = 1;
        while(!DHT && (++timer));
        timer = 1;
        while(DHT && (++timer));
        RH_H = getByte();
        RH_L = getByte();
        temp_H = getByte();
        temp_L = getByte();
        cksum = getByte();
        //8bit的湿度整数数据 + 8bit的湿度小数数据 + 8bit的温度整数数据 + 8bit的温度小数数据 等于所得结果的末8位校验位
        tmp = temp_H + temp_L + RH_H + RH_L;
        DHT = 1;
        if(tmp == cksum) {	
            gTempe = temp_H;
            gHuimi = RH_H;
						Send_Data_To_UART0(gTempe);						
						Send_Data_To_UART0(gHuimi);						
						return	0;
        }
    }
		return	1;
}


/*
*  @brief  10us延时
*  @param[in] 
*  @return	
*  @note
*/
void Delay10us(void)
{
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_();
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); 
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_();
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_();
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); 
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_();
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_();
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); 
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_();
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_();
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); 
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_();
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_();
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); 
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_();
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_();
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); 
    _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_();
	_nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_(); _nop_();
}


#endif	//温湿度传感器