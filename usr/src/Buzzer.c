#include	"N76E003.h"
#include  "SFR_Macro.h"
#include  "Define.h"
#include	"WS281X.h"
#include	"PWM.h"
#include  "Buzzer.h"
#include  "Timer.h"
#include  "I2C.h"
#include  "config.h"

#ifdef MOD_BUZZER																				//蜂鸣器数据处理函数

#define		BUZZER_TIME_OVER			(0xfa)									//时间溢出位,超过就一直响	
UINT8 State_Transformation = 0;													//开关蜂鸣器标志位
/*
*  @brief  有新数据来的时候初始化蜂鸣器
*  @param[in]  
*  @return
*  @note
*/
void buzzer_show_init(void)
{
		if(1 == get_rx_flag())	{																	//接收数据正确需要处理的标志位				
				clear_rx_flag();																			//显示效果标志位						
				State_Transformation = 0;															//开关蜂鸣器标志位,0打开
//				Time1flag = 0;																			//50ms进一次中断，总显示效果时间
//				Interval = 0;																				//计算总时间内频率的延时单位
		}
}
/*
*  @brief  	判断蜂鸣器是否需要处理数据
*  @param[in]  
*  @return	有运行返回1，没运行返回0
*  @note
*/
static UINT16 volume_save = 0;																//音量保存
static UINT8 buzzer_duration = 0;															//持续时间
static UINT8 buzzer_interval = 0;															//次数
static UINT8 buzzer_Pattern  = 0;															//模式
static UINT8 buzzer_time_interval = 0;												//时间间隔	
static UINT8 buzzer_begin = 0;																//1为工作
static UINT8 xdata ring_num = 0;															//蜂鸣器响的次数
/*
*  @brief  	延时刷新
*  @param[in]  
*  @return	
*  @note
*/
UINT8 get_buzzer_time_interval(void)
{
		return buzzer_time_interval;
}
/*
*  @brief  	led刷新显示
*  @param[in]  
*  @return	
*  @note
*/
static UINT8 buzzer_update_flag = 0;															//cled刷新显示
void set_buzzer_update_flag(void)
{
		buzzer_update_flag = 1;
}
/*
*  @brief  	04模式寄存器和08数据寄存器处理
*  @param[in]  
*  @return	
*  @note
*/
void buzzer_data_detection()																	
{
		if(1 == get_rx_flag()) {
				if(SYS_MODE_REG == get_current_register())	{								//04寄存器改模式
						buzzer_duration = store_4[0];														//持续时间
						buzzer_interval = store_4[1];														//次数
						buzzer_Pattern  = store_4[2];														//模式	
/*******************************************************************************
次数决定间隔时间，总时长cled_duration*2，定时器50ms一次，以100ms为单位要*2
闪的次数cled_interval，因为是闪停，所以*2
*******************************************************************************/					
						buzzer_time_interval = ((buzzer_duration<<1)/(buzzer_interval<<1));			//时间间隔
						if(WORK_MARK == buzzer_begin)	{																	//工作期间如果更换模式则按新模式重新开始
								buzzer_off();
								buzzer_show_init();
								ring_num = 0;
						}
						clear_rx_flag();																				//改完04模式寄存器退出
				}		
				if(SYS_DATA_REG == get_current_register())	{								//音量
						module_work();																					//执行周期内02寄存器数值为工作标记
						volume_save = get_buzzer_volume(&store_8[0]);
						buzzer_show_init();
						ring_num = 0;
						buzzer_begin = WORK_MARK;																				//开始工作
				}
		}
}
/*
*  @brief  	蜂鸣器PATTERN_ON模式
*  @param[in]  
*  @return	
*  @note
*/
UINT8 buzzer_pattern_on(void)
{		
		if(buzzer_duration < BUZZER_TIME_OVER) {																			//没有超出溢出时间需要停止	
				if(buzzer_update_flag == 0) {    																//时间间隔
							return 0;
				}
/*******************************************************************************
	闪烁次数够了退出,不然可能会出现频率不对
*******************************************************************************/							
				if(ring_num == buzzer_interval) {
						buzzer_off();
						module_rest();																										//执行周期结束02寄存器为0x00	
						buzzer_begin = REST_MARK;
						return 1;
				}					
				if(State_Transformation == 0)	{																				//打开蜂鸣器																					
//						if(Tim0flag >= 5) {																																		
								buzzer_on(&volume_save);									
//								Tim0flag = 0;
								ring_num++;	
//								Interval = 0;
								State_Transformation = 1;
//						}
				}else {																																//关闭蜂鸣器
						buzzer_off();									
//						Interval = 0;
						State_Transformation = 0;
				}
				buzzer_update_flag = 0;
		}else {																																				//持续时间大于溢出值则一直工作
				buzzer_on(&volume_save);	
				buzzer_begin = REST_MARK;
				return 1;			
		}
		return 0;
}
/*
*  @brief  	蜂鸣器处理数据
*  @param[in]  
*  @return	有运行返回1，没运行返回0
*  @note
*/
UINT8 buzzer_task(void)
{																						
		buzzer_data_detection();
		if(WORK_MARK != buzzer_begin) {																					//蜂鸣器响应
				return 0;
		}
		switch(buzzer_Pattern)
		{
			case PATTERN_OFF:																											//关闭
					buzzer_off();
					break;
			case PATTERN_ON:
					buzzer_pattern_on();																							//开启
					break;
			case PATTERN_FLASHING:
					break;
			case PATTERN_GRADIENT:
					break;
			default:
					break;	
		}	
		return 1;
}

#endif	//蜂鸣器数据处理函数		





















