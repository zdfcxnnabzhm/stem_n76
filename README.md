## stem:
通过n76的iic连接控制各模块

按键模块和蜂鸣器模块

蜂鸣器模块

i2cget -y 0 0x60 0x00							读寄存器
i2cget -y 0 0x60 0x02
i2cget -y 0 0x60 0x03
i2cget -y 0 0x60 0x04
i2cget -y 0 0x60 0x05
i2cget -y 0 0x60 0x08 

i2cset -y 0 0x60 0x04 0x50 0x04 0x01 0xf0 i		写04寄存器
i2cset -y 0 0x60 0x04 0xa0 0x06 0x01 0xf0 i
i2cset -y 0 0x60 0x04 0x50 0x04 0x01 i
i2cset -y 0 0x60 0x04 0xa0 0x06 0x01 i
i2cset -y 0 0x60 0x04 0x28 0x06 0x01 i
有带ID和不带ID，带ID会检测是否正确
格式为i2cset -y 0 0x60 0x04 + 总时间 + 频率 + 模式 （+ID） i


i2cset -y 0 0x60 0x08 0x01 0xf1 i				写08寄存器
i2cset -y 0 0x60 0x08 0x00 0xf0 i
i2cset -y 0 0x60 0x08 0x10 i
i2cset -y 0 0x60 0x08 0x01 i
i2cset -y 0 0x60 0x08 0x00 i
有带ID和不带ID，带ID会检测是否正确
格式为i2cset -y 0 0x60 0x08 + 音量（0~100） （+ID） i


按键模块和蜂鸣器模块

i2cget -y 0 0x18 0x00							读寄存器
i2cget -y 0 0x18 0x02
i2cget -y 0 0x18 0x04
i2cget -y 0 0x18 0x08 
i2cget -y 0 0x18 0x03 
i2cget -y 0 0x18 0x05 

08寄存器存储按键按下次数


i2cset -y 0 0x18 0x05 0x02 0x05 0x11 i			写05寄存器
格式为i2cset -y 0 0x18 0x05 + 大于小于 + 比较数值 + 达到要求时写入的值 （+ID） i 
01代表小于，02代表大于，比较数值和08寄存器的按键按下次数对比，达到了则将 达到要求时写入的值 写入02寄存器
02寄存器可以被上位机清0，清0后08寄存器存储的按键按下次数也会归0











