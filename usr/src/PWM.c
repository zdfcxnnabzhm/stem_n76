#include 	"N76E003.h"
#include 	"SFR_Macro.h"
#include 	"Common.h"
#include 	"Define.h"
#include  "config.h"
#include	"PWM.h"


#ifdef MOD_LED						//彩灯用到的函数
/*
*  @brief  初始化三路pwm
*  @param[in] 
*  @return
*  @note
*/
#define	LED_PWM_CYCLE_H    0x07
#define	LED_PWM_CYCLE_L    0xcf
#define U8_RGB						 0xff																				//rgb，255
/*
*  @brief  设置单色led的pwm值，将led的rgb三个引脚需要的颜色接到PWM引脚
*  @param[in]  
*  @return
*  @note
*/
void led_pwm_init(void)
{			
		PWM4_P01_OUTPUT_ENABLE;

		PWM_IMDEPENDENT_MODE;
		PWM_CLOCK_DIV_8;
		SetPWMPDat(LED_PWM_CYCLE_H,LED_PWM_CYCLE_L);
/**********************************************************************
PWM frequency = Fpwm/((PWMPH,PWMPL) + 1) <Fpwm = Fsys/PWM_CLOCK_DIV> 
							= (16MHz/8)/(0x7CF + 1)
							= 1KHz (1ms)
***********************************************************************/	
		SetPWM4Dat(0,0);					//初始化rgb灯数据，开始状态为不亮									
//-------- PWM start run--------------
		set_LOAD;
		set_PWMRUN;
}
/*
*  @brief  设置三路pwm的值，暂时用不到
*  @param[in]  
*  @return
*  @note
*/
void pwm_set(UINT8 rh,UINT8 rl,UINT8 gh,UINT8 gl,UINT8 bh,UINT8 bl)
{
		SetPWM2Dat(rh,rl);
/**********************************************************************
		set_SFRPAGE和clr_SFRPAGE是为了可以设置PWM4和PWM5
/***********************************************************************/	
		SetPWM4Dat(gh,gl);
		SetPWM5Dat(bh,bl);
/**********************************************************************
		LOAD寄存器置1，PWM才能载入新周期和占空比，更改结束后会硬件置0
***********************************************************************/		
		set_LOAD;
}
/*
*  @brief  设置单路pwm的值
*  @param[in]  
*  @return
*  @note
*/
void led_pwm_set(UINT8 color_h,UINT8 color_l)
{
		SetPWM4Dat(color_h, color_l);
		set_LOAD;
}

/*
*  @brief  获取led的pwm中255每一格所占的pwm大小
*  @param[in]  
*  @return
*  @note
*/
static float color_one_pwm = 0;
void get_led_pwm_min(void)
{
		float color_pwm = 0;
		color_pwm = (((LED_PWM_CYCLE_H)<<8)|LED_PWM_CYCLE_L);
		color_one_pwm = color_pwm/U8_RGB;
}

/*
*  @brief  设置单色led的pwm的数值
*  @param[in]  
*  @return
*  @note
*/
void led_pwm_Trans(UINT8* color)
{
		UINT16 pwmz = 0;
		UINT8 pwmh = 0, pwml = 0;
		
		pwmz = (*color)*color_one_pwm;
		pwmh = (pwmz & 0xff00) >> 8;
		pwml = pwmz & 0x00ff;
			
		led_pwm_set(pwmh, pwml);	
}

/*
*  @brief  关闭单色led灯
*  @param[in]  
*  @return
*  @note
*/
void led_off(void)
{
		led_pwm_set(0x00, 0x00);	
}
#endif											//led单色灯

#ifdef MOD_BUZZER					//蜂鸣器函数
/*
*  @brief  蜂鸣器pwm初始化，p10
*  @param[in]  
*  @return
*  @note
*/
#define	BUZZER_PWM_CYCLE_H    0x07
#define	BUZZER_PWM_CYCLE_L    0xcf
#define VOLUME_MAX						0x64						//音量100
void buzzer_pwm_init()
{
		P10_PushPull_Mode;
		PWM2_P10_OUTPUT_ENABLE;	
		PWM_IMDEPENDENT_MODE;
		PWM_CLOCK_DIV_8;
		SetPWMPDat(BUZZER_PWM_CYCLE_H,BUZZER_PWM_CYCLE_L);
/**********************************************************************
PWM frequency = Fpwm/((PWMPH,PWMPL) + 1) <Fpwm = Fsys/PWM_CLOCK_DIV> 
							= (16MHz/8)/(0x7CF + 1)
							= 1KHz (1ms)
***********************************************************************/
		SetPWM2Dat(0,0);
			set_LOAD;
			set_PWMRUN;
}
/*
*  @brief  关闭蜂鸣器
*  @param[in]  
*  @return
*  @note
*/
void buzzer_off()
{
		SetPWM2Dat(0,0);
		set_LOAD;
}
/*
*  @brief  获取一格音量的大小,因为 / 耗费时间长，所以单独出来 
*  @param[in] 
*  @return
*  @note
*/
static float volume_one_lattice = 0;
void get_buzzer_volume_min(void)
{
		float volume = 0;
		volume = (((BUZZER_PWM_CYCLE_H-1)<<8)|BUZZER_PWM_CYCLE_L);
		volume_one_lattice = volume/VOLUME_MAX;
}
/*
*  @brief  获取蜂鸣器音量的PWM
*  @param[in] temp,音量，0~100 
*  @return	返回蜂鸣器音量的PWM
*  @note
*/
UINT16 get_buzzer_volume(UINT8* temp)
{
		UINT16 pwz = 0;
/**********************************************************************
强制转换最大为0x06cf，音量volume_i最高100，temp为上位机发送的音量
***********************************************************************/
		if(*temp > VOLUME_MAX)
			*temp = VOLUME_MAX;	
		pwz = (*temp) * volume_one_lattice;
		return pwz;
}
/*
*  @brief  设置蜂鸣器音量
*  @param[in] 
*  @return	
*  @note
*/
void buzzer_on(UINT16 *temp)
{
		UINT8 pwh = 0,pwl = 0;												
		pwh = ((*temp) & 0xff00)>>8;	
		pwl = (*temp)&0x00ff;	
		SetPWM2Dat(pwh,pwl);	
		set_LOAD;
}
#endif															//蜂鸣器函数

#ifdef MOD_SERVO					//舵机
/**********************************************************************
	250度模拟舵机用50HZ，         360度数字舵机用300HZ
	PWM_CLOCK_DIV_128;						PWM_CLOCK_DIV_32;
	PWMPH = 0x09;									PWMPH = 0x06;
	PWMPL = 0xd3;									PWMPL = 0x8d;
	360度数字舵机：500us~1500us正转(00e0~02c0)越小越快，1500~2500反转(030f~04ff)越打越快	
		PWM2H = 0x02;						
		PWM2L = 0xff;				1500us舵机停止
***********************************************************************/
void pwm_servo_init()
{
		PWM2_P10_OUTPUT_ENABLE;
		PWM_IMDEPENDENT_MODE;
#if 1													//250度
		PWM_CLOCK_DIV_128;
		SetPWMPDat(0x09,0xd3);	
		SetPWM2Dat(0x00,0x00);
#endif
#if 0													//360度
		PWM_CLOCK_DIV_32;
		SetPWMPDat(0x06,0x8d);	
		SetPWM2Dat(0x02,0xff);
#endif	
	  set_LOAD;
    set_PWMRUN;
}
#endif		//舵机
