#ifndef _CLED_H_
#define	_CLED_H_

UINT8 cled_task(void);
UINT8 get_cled_time_interval(void);
void set_cled_update_flag(void);

#endif