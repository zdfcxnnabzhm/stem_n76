#ifndef _TIME_H_
#define _TIME_H_
#include "Config.h"

#define TH0_INIT        1300 
#define TL0_INIT        1300					//��ʱ��1ms

//extern UINT16 Time1flag;
//extern UINT16 Tim0flag;
//extern UINT16 Interval;

void timer0_init();
void timer1_init();

#ifdef MOD_ULTRA	
UINT8 get_sonar_delay_end(void);
UINT8 get_th0(void);
UINT8 get_tl0(void);
void  sonar_delay_clear(void);
#endif


#endif //_TIME_H_