#include "N76E003.h"
#include <intrins.h> 
#include "SFR_Macro.h"
#include "Define.h"
#include "WS281X.h"
#include "Timer.h"
#include "I2C.h"
#include "config.h"
#include "Sonar.h"


#ifdef MOD_ULTRA	
/*
*		HC_SR04的探测范围为2cm至400cm，距离/(1us的声波距离mm*1周期的us时间) 
*		最大探测距离为 1000/(0.17*1.0851)*4 = 21684
*/
#define iMAX_LEN 21684														
#define iMIN_LEN 109
/*			超出量程08寄存器变为0xfa				*/
#define ULTRA_OUT_RANGE						0xfa

sbit RX = P1^1; 																												//echo回波引脚							
sbit TX = P1^0;																													//trig触发信号控制端

static UINT16 miDistance = 0; 																					//差距距离
static UINT8 mcDistanceErr = 0; 																				//超声波状态位
static UINT8 ultra_flag = 1;																						//测距模块标志位启动标志位，0启动，1不启动
/*
*  @brief   获取最近一次测得的距离
*  @param[in] 
*  @return
*  @note
*/
UINT16 getDistance(void)
{
		return miDistance;
}
/*
*  @brief   获取最近一次测距的状态，0正常，1太近，2超量程
*  @param[in] 
*  @return
*  @note
*/
UINT16 getDistanceState(void)
{
		return mcDistanceErr;
}
/*
*  @brief   测距，测得的距离通过getDistanceState()和getDistance()获得结果
*  @param[in] 
*  @return
*  @note		https://blog.csdn.net/hzbigdog/article/details/6839828
*/
UINT8 refreshDistance(void)
{
		UINT16 i; 
		UINT16 iCycle; 																						//存储消耗的时间
		static UINT8 kklt = 0;																		//测距结束，1已结束，接收测得的数据，根据th0和thl的值判断
		if(1 == get_sonar_delay_end()) {													//判断是否在延迟期
				if(0 == ultra_flag) {
						i = iMAX_LEN; 																				//置入最大量程
						StartModule(); 																				//测距模块启动
						ultra_flag = 1;																				
				}
		}
		else 
				return 1;
		
		if(1 == ultra_flag) {																			//因为上位机随时可能访问mcu，所以不用while等待，用if延时
				if(!RX && ((i--) > 0));
				else 
						kklt = 1;
				
				if(1 == kklt) {
						if(i > 0) {
							TR0 = 1; 
							if( !RX ) {																						//RX为0，测距结束
									ultra_flag = 0;
									kklt = 0;			
									
									TR0=0; 																						//关闭定时器才能拿TH0和TL0的数据
									iCycle = (TH0 * 256 + TL0) + 1;										//计算总消耗的周期
									TH0 = 0;
									TL0 = 0;	
									
									if (iCycle <= iMIN_LEN) {
											mcDistanceErr = 1;														//距离太近，重启延时时间
											sonar_delay_clear(); 
											TH0 = get_th0(); 
											TL0 = get_tl0(); 
											TR0 = 1; 
									}else {																						
											miDistance = (UINT8)(iCycle * 0.01844670);		//1.0851 * 0.17 / 10
											mcDistanceErr = 0;
											sonar_delay_clear(); 
											TH0 = 0; 
											TL0 = 0; 
											TR0=1;
									}
							}
						}
				}else {
						mcDistanceErr = 2; 																				//超量程	
				}			
				return 0; 																										//测距完成
		}else { 
				return 1;																										
		}
}
/*
*  @brief   测距，测得的距离通过getDistanceState()和getDistance()获得结果
*  @param[in] 
*  @return
*  @note
*/
void ultra_task(void)
{
		static UINT16 ild = 0;																						//保存距离
		if(0 == refreshDistance()) {
				if(0 == getDistanceState() && (ild != getDistance())) {			  //与上次的值相比，改变了02寄存器置1
						ild = getDistance();			
						if((ild & 0x00FF)>0x03 && (ild & 0x00FF)<0x4f )	{					//检测距离为3cm至79cm，超过不太准，所以只取这些距离
								store_8[0] = ild & 0x00FF ;
								module_work();																				//距离改变02寄存器为1
//								Time1flag = 0;
								Send_Data_To_UART0(store_8[0]);
								ultra_flag = 1;
						}else {																										//超过距离则08寄存器变为0xfa
								ultra_flag = 0;	
								store_8[0] = ULTRA_OUT_RANGE;
						}			
				}
		}	
#if 0		
		if(Time1flag >= 10) {
				Time1flag = 0;
				module_rest();																								//500ms内距离一直没改变02寄存器为0
		}
#endif
}

/*******************************************************************************
启动超声波模块，需要保持22us的高电平
*******************************************************************************/
void StartModule(void)
{
	TX = 1;
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
		_nop_();_nop_();_nop_();_nop_();
	TX=0;
}

#endif
