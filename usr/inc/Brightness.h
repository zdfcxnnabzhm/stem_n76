#ifndef _BRIGHTNESS_H_
#define _BRIGHTNESS_H_


void brightness_init(void);
void brightness_task(void);
void adc_need_detection(void);

#endif