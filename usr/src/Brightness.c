#include "N76E003.h"
#include "SFR_Macro.h"
#include "Define.h"
#include "WS281X.h"
#include "Timer.h"
#include "I2C.h"
#include "Config.h"
#include "Brightness.h"

#ifdef MOD_ADC



/**********************************************************************
	光敏初始化，P05接D0,P17接A0进行AD转换
***********************************************************************/
void brightness_init(void)
{
		P05_PushPull_Mode;					
		Enable_ADC_AIN0;						// Enable AIN0 P1.7 as ADC input, Find in "Function_define.h" - "ADC INIT"
}
/*
*  @brief		05寄存器比较特殊，有三个值，第0位1为是代表小于，2是大于，第1位是代表要比较的数值，
						第2位是代表满足条件后02寄存器要写入的值，
*  @param[in] 
*  @return	
*  @note
*/
void brightness_default(UINT16* adc)
{
	static UINT8 i;
	if(get_stat_reg() == REST_MARK) {						//如果02寄存器已经被置0则不进入判断
		switch(store_5[0])
		{
			case DEFA_LESS:																		//*16是因为iic传输的数值最大为8位255，n76的adc是12位	
						if((((UINT16)store_5[1])*16) <= (*adc))	{		//连续取多次进行比较，如果中间有不对的说明数据错误或者不准确，归0
								i++;
						}else {
								i = 0;
						}
						if(i >= 3)
								set_stat_reg(store_5[2]);
						break;
			case DEFA_OVER:
						if((((UINT16)store_5[1])*16) >= (*adc)) {
								i++;
						}else {
								i = 0;
						}
						if(i >= 3)
								set_stat_reg(store_5[2]);							
						break;
			default:
						break;					
		}
	}
}
/*
*  @brief		光敏检测标记位置1，需要检测
*  @param[in] 
*  @return	
*  @note
*/
UINT8 adc_detection = 0;
void adc_need_detection(void)
{
		adc_detection = 1;
}
/*
*  @brief		返回光敏检测标记位，1需要检测
*  @param[in] 
*  @return	
*  @note
*/
UINT8 get_adc_detection(void)
{
		return adc_detection;
}
/**********************************************************************
	ADC取光面传感器数值，16次数据取平均值，较稳定
***********************************************************************/
static UINT16 xdata ADCdataH[16], ADCdataL[16], ADCsumH=0, ADCsumL=0;
static UINT8 ADCavgH = 0, ADCavgL = 0;
static UINT8 ADCRGBH = 0, ADCRGBL = 0; 
void brightness_task(void)
{
		UINT8 i;
		UINT16 bri_def = 0; 														//光敏比较
		if(get_adc_detection() == 0) {
				return ;
		}	
		
		store_8[0] = (ADCsumH/16)>>4;									//每次数据存放到0x08寄存器
		store_8[1] = ((ADCsumH/16)<<4)|(ADCsumL/16);
//							Send_Data_To_UART0(store_8[0]);	
//							Send_Data_To_UART0(store_8[1]);	
		bri_def = (store_8[0]<<8)|store_8[1];
		brightness_default(&bri_def);
		ADCsumH = 0x0000;
		ADCsumL = 0x0000;					
		for(i = 0; i < 16; i++) {											//取16次数据求平均值，较准确
				clr_ADCF;																	//ADC中断关闭
				set_ADCS;																	// ADC start trig signal
				while(ADCF == 0);
				ADCdataH[i] = ADCRH;
				ADCdataL[i] = ADCRL;
		}			
		for(i=0;i<16;i++) {
				ADCsumH = ADCsumH + ADCdataH[i];
				ADCsumL = ADCsumL + ADCdataL[i];
		}	
		
		adc_detection = 0;
}

#endif