#ifndef _SERVO_H_
#define _SERVO_H_


void servo_task(void);
UINT8 get_servo_delay(void);
void set_servo_need_down(void);

#endif