/********************************** (C) COPYRIGHT *******************************
* File Name          : IIC.H											*	
* Author             : OAZON                                                   *
* Version            : V1.0                                                    *
* Date               : 2018/06/15                                              *
* Description        : 
********************************************************************************/
#ifndef	_I2C_H_
#define _I2C_H_
/*
			00~08寄存器
*/
#define SYS_TYPE_REG (0x00)																			
#define SYS_CTRL_REG (0x01)
#define SYS_STAT_REG (0x02)
#define SYS_MARK_REG (0x03)
#define SYS_MODE_REG (0x04)
#define SYS_DEFA_REG (0x05)
#define SYS_DATA_REG (0x08)
/*
		iic接收数据数组大小
*/
#define SYS_RECE_NUM (6)
#define SYS_STOR_NUM (6)
/*
		1代表模块运行中，0代表模块没运行
*/
#define WORK_MARK		 (1)
#define REST_MARK		 (0)
/*
		05寄存器，01代表小于，02代表大于
*/
#define  DEFA_LESS	 (0x01)	
#define  DEFA_OVER	 (0x02)
/*
		00，01，02，03模式
*/
enum {
			PATTERN_OFF,
			PATTERN_ON,
			PATTERN_FLASHING,
			PATTERN_GRADIENT,
};	
 
//extern UINT8 volatile Rxflag;

//extern UINT8 I_register[8];
extern UINT8 store_4[3];
extern UINT8 store_5[3];
extern UINT8 store_8[3];
//extern UINT8 mode_save ;

UINT8 get_rx_flag(void);
void clear_rx_flag(void);

/*		i2c有关的函数   */
void Init_I2C(void);
void I2C_Error(void);
void I2C_Process(UINT8 u8DAT);
void IIC1(void);


/*		模块状态表示（工作or休息） */
void module_work(void);
void module_rest(void);

void key_data(void);

/*		当前的上位机操作的寄存器   */
UINT8 get_current_register(void);
void empty_current_register(void);

/*		02状态寄存器    */
void set_stat_reg(UINT8 temp);
UINT8 get_stat_reg(void);


void i2c_task(void);
void i2c_key(void);
void i2c_buzzer(void);
void i2c_motor(void);
void i2c_cled(void);
void i2c_led(void);
void i2c_servo(void);
void i2c_adc(void);
void i2c_ultra(void);
void key_data(void);
void i2c_ir(void);

#endif
