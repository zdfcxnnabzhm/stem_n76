#include "N76E003.h"
#include "Motor.h"
#include "SFR_Macro.h"
#include "Define.h"
#include "WS281X.h"
#include "I2C.h"
#include "Config.h"

#ifdef MOD_MOTOR																//电机
#define			MOTOR_STOP					(0)							//电机关
#define			MOTOR_RIGHT					(1)							//电机正转
#define			MOTOR_LEFT					(2)							//电机反转
/*******************************************************************************
		控制电机正反转，P11正，P12反，低电平转动
*******************************************************************************/
void motor_init(void)
{
	P11_PushPull_Mode;
	P12_PushPull_Mode;
	P11 = 1;
	P12 = 1;
}
/*
*  @brief  改变电机的两个引脚
*  @param[in]  left_pin为1，right_pin为0左转；right_pin为1，left_pin为0，右转；都为1停止
*  @return	
*  @note
*/
void motor_pin_control(UINT8 left_pin,UINT8 right_pin)
{
		P11 = left_pin;
		P12 = right_pin;
}
/*
*  @brief  电机执行数据
*  @param[in] 
*  @return	
*  @note
*/
void motor_task(void)
{					
	if(1 == get_rx_flag()) {
		clear_rx_flag();
		if(get_current_register() == SYS_MODE_REG) {
			switch(store_4[0])														//判断电机的执行效果，停止正反转
			{
					case MOTOR_STOP:
								motor_pin_control(1, 1);						//电机停止					
								module_rest();											//不转动的时候02寄存器为0，转动的时候为1
								break;
					case MOTOR_RIGHT:
								motor_pin_control(0, 1);						//电机右转													
								module_work();
								break;
					case MOTOR_LEFT:
								motor_pin_control(1, 0);						//电机左转
								module_work();
								break;
					default:	
								break;
			}	
		}
	}
}

#endif