#include "N76E003.h"
#include "SFR_Macro.h"
#include "Define.h"
#include "WS281X.h"
#include "I2C.h"
#include "Config.h"
#include "Servo.h"
#include "Timer.h"
#include "PWM.h"

#ifdef MOD_SERVO																//舵机
#define			SERVO_STOP					(0)							//电机关
#define			SERVO_LEFT					(1)							//电机正转
#define			SERVO_RIGHT					(2)							//电机反转
/**********************************************************************
	360度舵机停止
***********************************************************************/
void servo_stop()
{
		SetPWM2Dat(0x02, 0xff);
		set_LOAD;
}
/**********************************************************************
	360度舵机左转
***********************************************************************/
void servo_left()
{
		SetPWM2Dat(0x01, 0x00);			
		set_LOAD;
}
/**********************************************************************
	360度舵机右转
***********************************************************************/
void servo_right()
{
		SetPWM2Dat(0x03, 0x56);	
		set_LOAD;
}
/*
*  @brief  	延时刷新
*  @param[in]  
*  @return	
*  @note
*/
UINT8 servo_delay = 30;
UINT8 get_servo_delay(void)
{
		return servo_delay;
}
/*
*  @brief  	延时后pwm需要关闭
*  @param[in]  
*  @return	
*  @note
*/
static UINT8 servo_need_down = 0;															//cled刷新显示
void set_servo_need_down(void)
{
		servo_need_down++;
}
/*
*  @brief		250舵机转动的pwm是从0x0040开始的
*  @param[in] 
*  @return	
*  @note
*/
UINT8 servo_flag = 0;
void servo_pwm_Trans(U16* temp)														//数据转换
{
		U16 	u16_servo_pwm  = 0;															//pwm
		U8 	xdata serh = 0,serl = 0;	

		if((*temp) >= 0x00fa)																	//舵机最多转到这个位置
			*temp = 0x00fa;																								
		u16_servo_pwm = 0x0040 + (*temp);														
		serh = ((u16_servo_pwm & 0xff00)>>8);
	  serl = (u16_servo_pwm & 0x00ff);

		SetPWM2Dat(serh,serl);																//填入pwm		
		set_LOAD;
		
		module_work();	
		
		servo_need_down = 0;																	//置0，							
		servo_flag = 1;
#if 0
		while(RGB_rl<=40);
		PWM2H = 0x00;						
		PWM2L = 0x00;
		set_LOAD;	
#endif
}
#if 0
/*
*  @brief		360舵机
*  @param[in] 
*  @return	
*  @note
*/
void servo_task(void)																				//360舵机
{					
		if(1 == Rxflag) {
				Rxflag = 0;
				if(SYS_MODE_REG == get_current_register()) {
						switch(store_4[0])																//判断电机的执行状态，停止正反转
						{
								case SERVO_STOP:
													servo_stop();												//舵机停止				
													module_rest();
													break;
								case SERVO_LEFT:
													servo_left();												//舵机左转
													module_work();
													break;
								case SERVO_RIGHT:
													servo_right();											//舵机右转
													module_work();
													break;
								default:	
													break;
						}				
				}
		}
}
#endif
#if 1
void servo_task(void)												
{					
		if(1 == get_rx_flag()) {
				U16 servo_pwm = 0;
				clear_rx_flag();
				if(store_4[0] == WORK_MARK) {
						if(SYS_DATA_REG == get_current_register()) {
/*******************************************************************************
					将发送过来的两个数字相加变为转动角度值
*******************************************************************************/
								servo_pwm = (store_8[0]<<8)|store_8[1];											
								servo_pwm_Trans(&(servo_pwm));	
						}			
				}
				empty_current_register();								//转动完后清0,不再进入，虽然上面Rxflag已经为0，可能会有其它情况，暂时先执行
		}
		if(1 == servo_flag) {												//可控舵机只需要一定时间的pwm，转动完后过40ms，pwm清0
//				if(Tim0flag >= 40) {
				if(2 != servo_need_down) {
						return ;
				}
				
				SetPWM2Dat(0, 0);
				set_LOAD;
				
				servo_flag = 0;
				module_rest();
//				}
		}
}
#endif







#endif