#ifndef _CONFIG_H_
#define _CONFIG_H_

#define DEBUG						

/**************************************************************************************************
定义模块类型
***************************************************************************************************/
//#define MOD_KEY							//按键
//#define MOD_SW							//开关
//#define MOD_LED							//LED单色灯
//#define MOD_CLED						//RGB灯
//#define MOD_BUZZER				 	//蜂鸣器
//#define MOD_IR							//红外传感器
//#define MOD_ADC							//光敏
//#define MOD_SERVO						//舵机
//#define MOD_MOTOR						//电机
//#define MOD_DHT11						//温湿度传感器
#define MOD_ULTRA						//超声波

/**************************************************************************************************
定义通信方式
***************************************************************************************************/
#define VIA_IIC
//#define VIA_UART
//#define VIA_SPI



/**************************************************************************************************
定义系统频率
***************************************************************************************************/
//IF NOT CLK == 4MHz
#define IRCF_16MHz



/**************************************************************************************************
模块类型关联的通信地址
***************************************************************************************************/
#ifdef MOD_SW
	#define IIC_ADDR 	(0x12<<1)
#endif

#ifdef MOD_KEY
	#define IIC_ADDR 	(0x18<<1)
#endif

#ifdef MOD_LED
	#define IIC_ADDR 	(0x64<<1)
#endif

#ifdef MOD_CLED
	#define IIC_ADDR 	(0x14<<1)
#endif

#ifdef MOD_ADC
	#define IIC_ADDR 	(0x44<<1)
#endif	

#ifdef MOD_MOTOR
	#define IIC_ADDR 	(0x51<<1)
#endif	

#ifdef MOD_BUZZER
	#define IIC_ADDR 	(0x60<<1)
#endif	
	
#ifdef MOD_SERVO
	#define IIC_ADDR 	(0x6c<<1)
#endif

#ifdef MOD_IR
	#define IIC_ADDR 	(0x20<<1)
#endif

#ifdef MOD_DHT11
	#define IIC_ADDR 	(0x22<<1)
#endif

#ifdef MOD_ULTRA	
	#define IIC_ADDR 	(0x1c<<1)
#endif

#endif

