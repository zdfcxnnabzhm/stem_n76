#ifndef _LED_H_
#define _LED_H_



UINT8 get_led_time_interval(void);
UINT8 led_task(void);
void set_led_update_flag(void);


#endif