#ifndef  _BUZZER_H_
#define  _BUZZER_H_


UINT8 buzzer_task(void);
UINT8 get_buzzer_time_interval(void);
void set_buzzer_update_flag(void);
	
	
#endif