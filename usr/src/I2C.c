/********************************** (C) COPYRIGHT *******************************
* File Name          : IIC.C											*	
* Author             : OAZON                                                   *
* Version            : V1.0                                                    *
* Date               : 2018/06/15                                              *
* Description        : 
********************************************************************************/
#include "N76E003.h"
#include "WS281X.h"
#include "SFR_Macro.h"
#include "define.h"
#include "intrins.h"
#include "string.h"
#include "I2C.h"
#include "Config.h"
#include "Key.h"

/*
		上位机发送数据给02,04,05,08寄存器最大长度，04和08可能会因为模块不同而改变长度
*/
#define		STAT_NUM_MAX	 (3)				//02寄存器数值 + 次数 + 校验位
#define		DEFA_NUM_MAX	 (5)				//05寄存器数值 + 大于小于 + 比较数值 + 满足条件时填入的数值 + 校验位
#define		MODE_NUM_MAX   (5)				//04寄存器数值 + 持续时间 + 次数 + 模式 + 校验位
#define		DATA_NUM_MAX   (3)				//08寄存器数值 + 大小 + 校验位

#define		MOTOR_MODE_NUM_MAX   (3)	//电机04寄存器只有三个数，04寄存器数值 + 转动方向 + 校验位
#define		SERVO_MODE_NUM_MAX   (3)	//舵机04寄存器只有三个数，04寄存器数值 + 转动方向 + 校验位
#define		SERVO_DATA_NUM_MAX   (4)	//舵机08寄存器只有三个数，04寄存器数值 + 转动方向 + 校验位
#define   CLED_DATA_NUM_MAX		 (5)	//彩灯08寄存器有5个数，08 + R + G + B +校验位

#define   DATA_READ			 (1)				//读取数据标志位
#define   DATA_WRITE		 (2)				//写入数据标志位

#define		register_num   (3)				//上位机接收mcu发送的寄存器数值最多的个数		  
/* Private variables ---------------------------------------------------------*/
U8 store_4[3] = {0x00,0x00,0x00};										//04寄存器
U8 store_5[3] = {0x00,0x00,0x00};										//05寄存器
U8 store_8[3] = {0x00,0x00,0x00};										//08寄存器
U8 store_1[3] = {0x00,0x00,0x00};										//01寄存器

U8 volatile data_received[SYS_RECE_NUM];						//中断里拿数据的数组
//U8 volatile data_store[SYS_STOR_NUM];							//接收数组，接收中断拿出来的数组
																						
U8 volatile data_num = 0;														//iic拿到的数据个数
U8 volatile Rxflag=0;																//接收数据正确需要处理的标志位
U8 volatile begin_flag = 0;													//发送结束位
U8 mode_save = 0;																		//保存寄存器数值，用于校验

U8 I_register[8] = {0xa0,0x00,0x00,0xf0,0x00,0x00,0x00,0x00};
U8 data_read[3];																		//上位机要读取的值
U8 data_low = 0;																		//上位机发送过来的数组第一个值
U8 ll = 0;																					

#ifdef	MOD_KEY
U8 key_state = 0;																		//按键事件状态标志位
#endif

/*
*  @brief  iic从机初始化，p14 sda,p13 scl	
*  @param[in] 
*  @return
*  @note
*/
void Init_I2C(void)
{
	P13_OpenDrain_Mode;
	P14_OpenDrain_Mode;		
	
    SDA = 1;                                						//set SDA and SCL pins high
    SCL = 1;   	
    set_P0SR_6;                           				  		//set SCL (P06) is  Schmitt triggered input select. 	
    set_EI2C;                              							//enable I2C interrupt by setting IE1 bit 0
    set_EA;
    I2ADDR = IIC_ADDR;                    							//define own slave address
    set_I2CEN;                              						//enable I2C circuit
    set_AA;
	
	memset(data_received,0x00,SYS_RECE_NUM);							//清零，防止后面处理数据时拿取的数据出错
//	memset(data_store,0x00,SYS_RECE_NUM);	
}
/*
*  @brief  iic中断处理函数
*  @param[in] 
*  @return
*  @note
*/
void I2C_ISR(void) interrupt 6
{
    switch (I2STAT)
    {
        case 0x60:												//ADDR ACK
            AA = 1;
            break;
#if 0				
        case 0x68:
						P02 = 0;
            while(1);
            break;
#endif
        case 0x80:												//DATA ACK				
            data_received[data_num] = I2DAT;
						data_num++;					
            AA = 1;
            break;		       
/*******************************************************************************
主机发送停止位，程序每次都会执行但是有时候判断不到，所以改为判断数据值
*******************************************************************************/
		case 0xA0:	//REC END		
						begin_flag = 1;	
            AA = 1;
            break;
		case 0xA8:	//ADDR BACK ACK					
						I2DAT = data_read[ll];
						ll++;
            AA = 1;
            break;  			
        case 0xB8:  //DATA BACK ACK 				
						I2DAT = data_read[ll];
						ll++;
            AA = 1;
            break;
        case 0xC0:						
            AA = 1;
            break; 
		default:	
			set_STO;
			break;		
    }
    SI = 0;
    while(SI != 0)
    {
        if(I2STAT == 0x00)
        {
            set_STO;
        }
        SI = 0;
        if(SI != 0)
        {
            clr_I2CEN;
            set_I2CEN;
            clr_SI;
            //clr_I2CEN
        } 
    }
    while(STO);  
}
/*
*  @brief  模块运行中02寄存器值为WORK_MARK
*  @param[in] 
*  @return 	
*  @note
*/
void module_work(void)
{
		I_register[SYS_STAT_REG] = WORK_MARK;
}
/*
*  @brief  模块休息中02寄存器值为REST_MARK
*  @param[in] 
*  @return 
*  @note
*/
void module_rest(void)
{
		I_register[SYS_STAT_REG] = REST_MARK;
}
/*
*  @brief  获取当前上位机访问的寄存器（08，04，05）
*  @param[in] 
*  @return 
*  @note
*/
UINT8 get_current_register(void)
{
		return mode_save;
}
/*
*  @brief  将当前上位机访问的寄存器（08，04，05）清0
*  @param[in] 
*  @return 
*  @note
*/
void empty_current_register(void)
{
		mode_save = 0;
}
/*
*  @brief  获取iic数据标志位（1代表正确，需要处理）
*  @param[in] 
*  @return 
*  @note
*/
UINT8 get_rx_flag(void)
{
		return Rxflag;
}
/*
*  @brief  清除数据标志位
*  @param[in] 
*  @return 
*  @note
*/
void clear_rx_flag(void)
{
		Rxflag = 0;
}
/*
*  @brief  改变02状态寄存器的数值，输出模块（按键，光敏等）
*  @param[in] 
*  @return 
*  @note
*/
void set_stat_reg(UINT8 temp)
{
		I_register[2] = temp;
}
/*
*  @brief  获取02状态寄存器的数值
*  @param[in] 
*  @return 
*  @note
*/
UINT8 get_stat_reg(void)
{
		return I_register[2];
}

/*
*  @brief  04,05,08寄存器处理数据
*  @param[in] 
*  @return 返回0则为错误，返回1表示上位机读。返回2表示上位机写	
*  @note
*/
UINT8 data_detection(UINT8 fact_length,UINT8 length_max)
{
		//超出最长的长度返回错误，等于则说明带ID，需要与03寄存器比较或者等于0才通过
		if(fact_length > length_max) {
				return 0;
		}else if(fact_length == length_max) {
				if(data_received[length_max-1] != I_register[SYS_MARK_REG] && data_received[length_max-1] != 0x00)
						return 0;
		}else if(1 == fact_length) {																				//1为上位机读取数值	
				return DATA_READ;
		}					
		return DATA_WRITE;																									//上位机写数据
}
/*
*  @brief  读or写处理函数
*  @param[in] 
*  @return 
*  @note
*/
void data_read_write(UINT8 flag,UINT8 mode,UINT8 length_max)
{
		UINT8 j;
		if(DATA_READ == flag) {														 														//上位机读取寄存器
				for(j = 0; j < register_num; j++) {
						switch(mode)
						{
								case SYS_MODE_REG:
										data_read[j]=store_4[j];
										break;
								case SYS_DEFA_REG:
										data_read[j]=store_5[j];
										break;
								case SYS_DATA_REG:
										data_read[j]=store_8[j];
										break;
								default:
										break;
						}
				}														
		}else if(DATA_WRITE == flag) {																								//上位机写数据
				mode_save = data_received[0];																							//存储04 or 05 or 08寄存器
				switch( mode )
				{
					case SYS_MODE_REG:						
							for(j = 0; j < length_max-2; j++) {
								 store_4[j] = data_received[j+1];																  //04寄存器3个数值
							}
							break;
					case SYS_DEFA_REG:							
							for(j = 0; j < length_max-2; j++) {
								 store_5[j] = data_received[j+1];																  //05寄存器3个数值
							}
							break;		
					case SYS_DATA_REG:
							for(j = 0; j < length_max-2; j++) {
								 store_8[j] = data_received[j+1];																  //08寄存器1个数值
							}							
							break;
					default:
							break;
				}				
				Rxflag = 1;	
		}				

}
/*
*  @brief  00,02,03寄存器处理数据
*  @param[in] 
*  @return 返回0则为错误，返回1表示上位机读。返回2表示上位机写	
*  @note
*/
void special_data_detection(UINT8 fact_length,UINT8 mode)
{
		switch( mode )
		{
				case SYS_TYPE_REG:
						if(1 == fact_length) {
								data_read[0] = I_register[mode];
						}
						break;
				case SYS_STAT_REG:
						if(2 == fact_length) {																								//写入值
								I_register[mode] = data_received[1];
								break;
						}else if(1 == fact_length) {																					//读取值
								data_read[0] = I_register[mode];	
						}						
						break;
				case SYS_MARK_REG:
						if(2 == fact_length) {
								I_register[mode] =  data_received[1];
								break;
						}else if(1 == fact_length) {				
								data_read[0] = I_register[mode];
						}
						break;
				default:
						break;
		}

}
/*
*  @brief  蜂鸣器处理iic拿到的数据
*  @param[in] 
*  @return
*  @note
*/
#ifdef MOD_BUZZER																													//蜂鸣器
void i2c_buzzer(void)																													
{
		UINT8 tmp = 0;
		if(1 == begin_flag)	 																											//主机发送数据结束标志
		{			
			ll = 0;
			data_low = data_received[0];	
			switch(data_low){
				case SYS_TYPE_REG:
							special_data_detection(data_num, SYS_TYPE_REG);
							break;
				case SYS_STAT_REG:
							special_data_detection(data_num, SYS_STAT_REG);
							break;									
				case SYS_MARK_REG:
							special_data_detection(data_num, SYS_MARK_REG);
							break;
				case SYS_MODE_REG:
							tmp = data_detection(data_num, MODE_NUM_MAX);		
							if( tmp ) {
									data_read_write(tmp, SYS_MODE_REG, MODE_NUM_MAX);
							}	
							break;
				case SYS_DATA_REG:
							tmp = data_detection(data_num,DATA_NUM_MAX);
							if( tmp ) {
									data_read_write(tmp, SYS_DATA_REG, DATA_NUM_MAX);
							}								
							break;	
				default:
							memset(data_read,0x00,register_num);
							break;
			}
			data_num = 0;
			memset(data_received,0x00,SYS_RECE_NUM);														//数据清除，防止错误
			begin_flag = 0;	
		}	
}
#endif		//蜂鸣器


#ifdef MOD_KEY																														//按键
/*
*  @brief		按键事件处理函数
*  @param[in] 
*  @return	
*  @note		08寄存器第0位记录按键按下次数，第1位记录长按次数
*/
void key_data(void)
{
		key_state = key_read();					
		switch(key_state)
		{
			case key_click:
					clr_GPIO1;										
					store_8[0]++;															//按键按下次数
					break;
			case key_double:
					set_GPIO1;
					store_8[0] = store_8[0]+2;							
					break;
			case key_long:
					store_8[0]++;
					store_8[1]++;		
					P12 = ~P12;
					break;
		}
}
void i2c_key(void)
{
		UINT8 tmp = 0;	
		if(1 == begin_flag)	 																									//主机发送数据结束标志
		{
				ll = 0;
				data_low = data_received[0];
				switch(data_low)
				{
					case SYS_TYPE_REG:
								special_data_detection(data_num, SYS_TYPE_REG);
								break;
					case SYS_STAT_REG:
								special_data_detection(data_num, SYS_STAT_REG);
#ifdef MOD_KEY		//当02寄存器被清0的时候，08寄存器记录的按键按下次数清0
								if(I_register[2] == 0x00) {
									store_8[0] = 0x00;
									store_8[1] = 0x00;
								}						
#endif
								break;						
					case SYS_MARK_REG:
								special_data_detection(data_num, SYS_MARK_REG);
								break;
					case SYS_MODE_REG:
								tmp = data_detection(data_num, MODE_NUM_MAX);
								if( tmp ) {
										data_read_write(tmp, SYS_MODE_REG, MODE_NUM_MAX);
								}	
								break;
					case SYS_DEFA_REG:
								tmp = data_detection(data_num, DEFA_NUM_MAX);
								if( tmp ) {
										data_read_write(tmp, SYS_DEFA_REG, DEFA_NUM_MAX);
								}					
								break;
					case SYS_DATA_REG:
								tmp = data_detection(data_num, DATA_NUM_MAX);
								if( tmp ) {
										data_read_write(tmp, SYS_DATA_REG, DATA_NUM_MAX);
								}	
								break;	
					default:
								memset(data_read, 0x00, register_num);
								break;
				}
				data_num = 0;
				memset(data_received, 0x00, SYS_RECE_NUM);													//数据清除，防止错误
				begin_flag = 0;	
		}					
}
#endif		//按键		

#ifdef MOD_ULTRA																													//超声波模块											
void i2c_ultra(void)
{
		UINT8 tmp = 0;	
		if(1 == begin_flag)	 																									//主机发送数据结束标志
		{
				ll = 0;
				data_low = data_received[0];
				switch(data_low)
				{
					case SYS_TYPE_REG:
								special_data_detection(data_num, SYS_TYPE_REG);
								break;
					case SYS_STAT_REG:
								special_data_detection(data_num, SYS_STAT_REG);
								break;						
					case SYS_MARK_REG:
								special_data_detection(data_num, SYS_MARK_REG);
								break;
					case SYS_MODE_REG:
								tmp = data_detection(data_num, MODE_NUM_MAX);
								if( tmp ) {
										data_read_write(tmp, SYS_MODE_REG, MODE_NUM_MAX);
								}	
								break;
					case SYS_DEFA_REG:
								tmp = data_detection(data_num, DEFA_NUM_MAX);
								if( tmp ) {
										data_read_write(tmp, SYS_DEFA_REG, DEFA_NUM_MAX);
								}					
								break;
					case SYS_DATA_REG:
								tmp = data_detection(data_num, DATA_NUM_MAX);
								if( tmp ) {
										data_read_write(tmp, SYS_DATA_REG, DATA_NUM_MAX);
								}	
								break;	
					default:
								memset(data_read, 0x00, register_num);
								break;
				}
				data_num = 0;
				memset(data_received, 0x00, SYS_RECE_NUM);														//数据清除，防止错误
				begin_flag = 0;	
		}					
}
#endif	//超声波模块

#ifdef MOD_ADC																														//光敏
void i2c_adc(void)
{
		UINT8 tmp = 0;	
		if(1 == begin_flag)	 																									//主机发送数据结束标志
		{
				ll = 0;
				data_low = data_received[0];
				switch(data_low)
				{
					case SYS_TYPE_REG:
								special_data_detection(data_num, SYS_TYPE_REG);
								break;
					case SYS_STAT_REG:
								special_data_detection(data_num, SYS_STAT_REG);
								break;						
					case SYS_MARK_REG:
								special_data_detection(data_num, SYS_MARK_REG);
								break;
					case SYS_MODE_REG:
								tmp = data_detection(data_num, MODE_NUM_MAX);
								if( tmp ) {
										data_read_write(tmp, SYS_MODE_REG, MODE_NUM_MAX);
								}	
								break;
					case SYS_DEFA_REG:
								tmp = data_detection(data_num, DEFA_NUM_MAX);
								if( tmp ) {
										data_read_write(tmp, SYS_DEFA_REG, DEFA_NUM_MAX);
								}					
								break;
					case SYS_DATA_REG:
								tmp = data_detection(data_num, DATA_NUM_MAX);
								if( tmp ) {
										data_read_write(tmp, SYS_DATA_REG, DATA_NUM_MAX);
								}	
								break;	
					default:
								memset(data_read, 0x00, register_num);
								break;
				}
				data_num = 0;
				memset(data_received, 0x00, SYS_RECE_NUM);														//数据清除，防止错误
				begin_flag = 0;	
		}					
}
#endif

#ifdef MOD_MOTOR																													//电机
void i2c_motor(void)
{
		UINT8 tmp = 0;	
		if(1 == begin_flag)	 																									//主机发送数据结束标志
		{
				ll = 0;
				data_low = data_received[0];
				switch(data_low)
				{
					case SYS_TYPE_REG:
								special_data_detection(data_num, SYS_TYPE_REG);
								break;
					case SYS_STAT_REG:
								special_data_detection(data_num, SYS_STAT_REG);
								break;						
					case SYS_MARK_REG:
								special_data_detection(data_num, SYS_MARK_REG);
								break;
					case SYS_MODE_REG:
								tmp = data_detection(data_num, MOTOR_MODE_NUM_MAX);
								if( tmp ) {
										data_read_write(tmp, SYS_MODE_REG, MOTOR_MODE_NUM_MAX);
								}	
								break;
					case SYS_DEFA_REG:
								tmp = data_detection(data_num, DEFA_NUM_MAX);
								if( tmp ) {
										data_read_write(tmp, SYS_DEFA_REG, MOTOR_MODE_NUM_MAX);
								}					
								break;
					case SYS_DATA_REG:
								tmp = data_detection(data_num, DATA_NUM_MAX);
								if( tmp ) {
										data_read_write(tmp, SYS_DATA_REG, MOTOR_MODE_NUM_MAX);
								}	
								break;	
					default:
								memset(data_read, 0x00, register_num);
								break;
				}
				data_num = 0;
				memset(data_received, 0x00, SYS_RECE_NUM);														//数据清除，防止错误
				begin_flag = 0;	
		}					
}
#endif     //电机
#ifdef MOD_SERVO																													//舵机																				
void i2c_servo(void)
{
		UINT8 tmp = 0;	
		if(1 == begin_flag)	 																									//主机发送数据结束标志
		{
				ll = 0;
				data_low = data_received[0];
				switch(data_low)
				{
					case SYS_TYPE_REG:
								special_data_detection(data_num,SYS_TYPE_REG);
								break;
					case SYS_STAT_REG:
								special_data_detection(data_num,SYS_STAT_REG);
								break;						
					case SYS_MARK_REG:
								special_data_detection(data_num,SYS_MARK_REG);
								break;
					case SYS_MODE_REG:
								tmp = data_detection(data_num,SERVO_MODE_NUM_MAX);
								if( tmp ) {
										data_read_write(tmp, SYS_MODE_REG, SERVO_MODE_NUM_MAX);
								}	
								break;
					case SYS_DEFA_REG:
								tmp = data_detection(data_num,DEFA_NUM_MAX);
								if( tmp ) {
										data_read_write(tmp, SYS_DEFA_REG, DEFA_NUM_MAX);
								}					
								break;
					case SYS_DATA_REG:
								tmp = data_detection(data_num,SERVO_DATA_NUM_MAX);
								if( tmp ) {
										data_read_write(tmp, SYS_DATA_REG, SERVO_DATA_NUM_MAX);	
								}	
								break;	
					default:
								memset(data_read,0x00,register_num);
								break;
				}
				data_num = 0;
				memset(data_received,0x00,SYS_RECE_NUM);														//数据清除，防止错误
				begin_flag = 0;	
		}					
}
#endif							//舵机

#ifdef MOD_CLED																														//彩灯
void i2c_cled(void)																														
{
		UINT8 tmp = 0;
		if(1 == begin_flag)	 																											//主机发送数据结束标志
		{			
			ll = 0;
			data_low = data_received[0];	
			switch(data_low){
				case SYS_TYPE_REG:
							special_data_detection(data_num, SYS_TYPE_REG);
							break;
				case SYS_STAT_REG:
							special_data_detection(data_num, SYS_STAT_REG);
							break;									
				case SYS_MARK_REG:
							special_data_detection(data_num, SYS_MARK_REG);
							break;
				case SYS_MODE_REG:
							tmp = data_detection(data_num, MODE_NUM_MAX);		
							if( tmp ) {
									data_read_write(tmp, SYS_MODE_REG, MODE_NUM_MAX);
							}	
							break;
				case SYS_DATA_REG:
							tmp = data_detection(data_num, CLED_DATA_NUM_MAX);
							if( tmp ) {
									data_read_write(tmp, SYS_DATA_REG, CLED_DATA_NUM_MAX);
							}								
							break;	
				default:
							memset(data_read,0x00,register_num);
							break;
			}
			data_num = 0;
			memset(data_received,0x00,SYS_RECE_NUM);														//数据清除，防止错误
			begin_flag = 0;	
		}	
}
#endif			//彩灯

#ifdef MOD_LED																														//单色led灯
void i2c_led(void)																														
{
		UINT8 tmp = 0;
		if(1 == begin_flag)	 																											//主机发送数据结束标志
		{			
			ll = 0;
			data_low = data_received[0];	
			switch(data_low){
				case SYS_TYPE_REG:
							special_data_detection(data_num, SYS_TYPE_REG);
							break;
				case SYS_STAT_REG:
							special_data_detection(data_num, SYS_STAT_REG);
							break;									
				case SYS_MARK_REG:
							special_data_detection(data_num, SYS_MARK_REG);
							break;
				case SYS_MODE_REG:
							tmp = data_detection(data_num, MODE_NUM_MAX);		
							if( tmp ) {
									data_read_write(tmp, SYS_MODE_REG, MODE_NUM_MAX);
							}	
							break;
				case SYS_DATA_REG:
							tmp = data_detection(data_num, DATA_NUM_MAX);
							if( tmp ) {
									data_read_write(tmp, SYS_DATA_REG, DATA_NUM_MAX);
							}								
							break;	
				default:
							memset(data_read,0x00,register_num);
							break;
			}
			data_num = 0;
			memset(data_received,0x00,SYS_RECE_NUM);														//数据清除，防止错误
			begin_flag = 0;	
		}	
}

#endif					//单色led灯
/************************ (C) COPYRIGHT OAZON ******************END OF FILE****/




















