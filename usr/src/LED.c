#include	"N76E003.h"
#include  "SFR_Macro.h"
#include  "Define.h"
#include	"WS281X.h"
#include	"PWM.h"
#include  "Timer.h"
#include  "I2C.h"
#include  "config.h"
#include  "LED.h"

#ifdef MOD_LED

#define		LED_TIME_OVER			(0xfa)													//时间溢出位,超过就一直响	
UINT8 State_Transformation = 0;															//开关单色灯标志位
/*
*  @brief  有新数据来的时候初始化单色灯
*  @param[in]  
*  @return
*  @note
*/
void led_show_init(void)
{
		if(1 == get_rx_flag())	{																	//接收数据正确需要处理的标志位			
				clear_rx_flag();																			//显示效果标志位						
				State_Transformation = 0;															//开关单色灯标志位,0打开
//				Time1flag = 0;																			//50ms进一次中断，总显示效果时间
//				Interval = 0;																				//计算总时间内频率的延时单位
		}
}
/*
*  @brief  	判断单色灯是否需要处理数据
*  @param[in]  
*  @return	有运行返回1，没运行返回0
*  @note
*/
static UINT8 led_duration = 0;																//持续时间
static UINT8 led_interval = 0;																//次数
static UINT8 led_Pattern  = 0;																//模式
static UINT8 led_time_interval = 0;														//时间间隔	
static UINT8 led_begin = 0;																		//1为工作
static UINT8 xdata ring_num = 0;															//闪的次数
/*
*  @brief  	延时刷新
*  @param[in]  
*  @return	
*  @note
*/
UINT8 get_led_time_interval(void)
{
		return led_time_interval;
}
/*
*  @brief  	led刷新显示
*  @param[in]  
*  @return	
*  @note
*/
static UINT8 led_update_flag = 0;															//led刷新显示
void set_led_update_flag(void)
{
		led_update_flag = 1;
}
/*
*  @brief  	04模式寄存器和08数据寄存器处理
*  @param[in]  
*  @return	
*  @note
*/
void led_data_detection()																	
{
		if(1 == get_rx_flag()) {
				if(SYS_MODE_REG == get_current_register())	{								//04寄存器改模式
						led_duration = store_4[0];															//持续时间
						led_interval = store_4[1];															//次数
						led_Pattern  = store_4[2];															//模式	
/*******************************************************************************
次数决定间隔时间，总时长led_duration*2，定时器50ms一次，以100ms为单位要*2
闪的次数led_interval，因为是闪停，所以*2
*******************************************************************************/						
						led_time_interval = ((led_duration<<1)/(led_interval<<1));			//时间间隔
						if(WORK_MARK == led_begin)	{																		//工作期间如果更换模式则按新模式重新开始
								ring_display_clear();
								led_show_init();
								ring_num = 0;
						}
						clear_rx_flag();																				//改完04模式寄存器退出
				}		
				if(SYS_DATA_REG == get_current_register())	{								//执行单色灯
						module_work();																					//执行周期内02寄存器数值为工作标记
						led_show_init();
						ring_num = 0;
						led_begin = WORK_MARK;																	//开始工作
				}
		}
}
/*
*  @brief  	单色灯PATTERN_ON模式
*  @param[in]  
*  @return	
*  @note
*/
UINT8 led_pattern_on(void)
{
		if(led_duration < LED_TIME_OVER) {																						//没有超出溢出时间需要停止										
				if(led_update_flag == 0) {    																		//时间间隔
						return 0;
				}	
/*******************************************************************************
闪烁次数够了退出,不然可能会出现频率不对
*******************************************************************************/							
				if(ring_num == led_interval) {
						led_off();
						module_rest();																										//执行周期结束02寄存器为0x00	
						led_begin = REST_MARK;
						return 1;
				}						
				if(0 == State_Transformation)	{																				//显示																					
//								if(Tim0flag >= 10) {					
								led_pwm_Trans(&(store_8[0]));
//										Tim0flag = 0;
//										Interval = 0;
								ring_num++;	
								State_Transformation = 1;
//								}
				}else {																																//关闭
						led_off();									
//								Interval = 0;
						State_Transformation = 0;
				}	
				
				led_update_flag = 0;

		}else {																																				//持续时间大于溢出值则一直工作
//				if(Tim0flag >= 30) {	
//						Tim0flag = 0;					
						WS_frame_asyn(&(store_8));	
						led_begin = REST_MARK;
//				}
				return 1;			
		}
		return 0;
}
/*
*  @brief  	单色灯处理数据
*  @param[in]  
*  @return	有运行返回1，没运行返回0
*  @note
*/
UINT8 led_task(void)
{																						
		led_data_detection();
		if(WORK_MARK != led_begin) {																		//单色灯响应
				return 0;
		}
		switch(led_Pattern)
		{
				case PATTERN_OFF:																							//关闭
						led_off();
						break;
				case PATTERN_ON:
						led_pattern_on();																					//开启
						break;
				case PATTERN_FLASHING:
						break;
				case PATTERN_GRADIENT:
						break;
				default:
						break;	
		}	
		return 1;
}





#endif